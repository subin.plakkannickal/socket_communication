import socket               # Import socket module
import thread
_socket = socket.socket()         # Create a socket object
host = socket.gethostname() # Get local machine name
port = 12345                # Reserve a port for your service.

_socket.connect((host, port))
print _socket.recv(1024)

def Receive(_socket):
    while True:
        print 'server: ', _socket.recv(1024)


thread.start_new_thread(Receive,(_socket,))